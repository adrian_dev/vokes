var controller = new ScrollMagic.Controller();

// var scroller = new ScrollMagic.Scene({
//   triggerElement: "#vr-man",
//   triggerHook: 1,
//   duration: "100%"
// }).setTween(TweenMax.from("#vr-man img", 1, {y: '-50%', ease: Power0.easeNone}))
// .addTo(controller);

$(function() {
  new ScrollMagic.Scene({
    triggerElement: ".section-3"
  }).on('enter', function() {
    $(".section-3 > .column:nth-of-type(1)").addClass('animate');
  }).addTo(controller);
  new ScrollMagic.Scene({
    triggerElement: ".section-3 > .column:nth-of-type(1)"
  }).on('enter', function() {
    $(".section-3 > .column:nth-of-type(2)").addClass('animate');
  }).addTo(controller);
  new ScrollMagic.Scene({
    triggerElement: ".section-3 > .column:nth-of-type(2)"
  }).on('enter', function() {
    $(".section-3 > .column:nth-of-type(3)").addClass('animate');
  }).addTo(controller);

  $(".section-3 > .column:nth-of-type(1) .item img").click(function() {
    window.top.location = "http://mmoser.com/vr/sites/1/AM_1_360.html";
  });
  $(".section-3 > .column:nth-of-type(2) .item img").click(function() {
    window.top.location = "http://mmoser.com/vr/sites/2/MFO_360_2.html";
  });
  $(".section-3 > .column:nth-of-type(3) .item img").click(function() {
    window.top.location = "http://www.mmoser.com/vr/sites/3/";
  });
});

// function reset(small) {
//   scroller.destroy();
//
//   if (small) {
//     $("#vr-man img").css({
//       transform: "perspective(50px) translate3d(0, 5px, 27px)"
//     });
//   } else {
//     $("#vr-man img").css({
//       transform: "perspective(50px) translate3d(0, 0, 9px)"
//     });
//   }
// }
